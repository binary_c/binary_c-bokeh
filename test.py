#!/usr/bin/env python3

import collections

xlist = [1,2,3]


class AutoVivificationDict(dict):
    """
    Implementation of perl's autovivification feature, by overriding the
    get item and the __iadd__ operator (https://docs.python.org/3/reference/datamodel.html?highlight=iadd#object.__iadd__)

    This allows to set values within a subdict that might not exist yet:

    Example:
        newdict = {}
        newdict['example']['mass'] += 10
        print(newdict)
        >>> {'example': {'mass': 10}}
    """

    def __getitem__(self, item):
        """
        Getitem function for the autovivication dict
        """

        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

    def __iadd__(self, other):
        """
        iadd function (handling the +=) for the autovivication dict.
        """

        # if a value does not exist, assume it is 0.0
        try:
            self += other
        except:
            self = other
        return self

for i in range(0,100000000):
    d = {}

    #def f():
    #    return 0
    #d = AutoVivificationDict()
    #d = collections.defaultdict(f)

    for x in xlist:
    # 34.18s
    #    if x in d:
    #        d[x] += 1
    #    else:
    #        d[x] = 1

    # 51.31s
    #d[x] = d.setdefault(x,0) + 1

        d[x] = d.get(x,0) + 1

    # AutoVivificationDict > 120s
    # defaultdict > 60s
    #d[x] += 1
