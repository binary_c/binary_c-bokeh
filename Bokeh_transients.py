#!/usr/bin/env python3

############################################################
#
# Make interactive Hertzsprung-Russell diagram using binary_c
# ensemble data made with binary_c and libcdict.
#
############################################################

import binarycpython
import bokeh
import ensemble_bokeh
import math
import traceback
import json
import sys
vb = True # verbose output flag
dist = ('transients')



############################################################
# Load in ensemble data and choose free parameter
############################################################
if vb:
    print("Load ensemble")
try:
    ensemble_file = sys.argv[1]
except:
    ensemble_file = '/var/tmp/ensemble_output.json'

# load all ensemble data
ensemble = binarycpython.utils.ensemble.load_ensemble(ensemble_file,quiet=True,allow_nan=True)['ensemble']

# choose vbar_E factor (the free parameter)
free_parameter = 'vbar_E_factor'
vbar_E_factor = 0.3

# select that required
all_times_data = ensemble[dist + ' Bokeh']['all times']['vbar_E_factor'][vbar_E_factor]


############################################################
# set up plots
############################################################
plot_title = f"luminous red novae with {free_parameter} = {vbar_E_factor}"
xcol = 'log timescale / d'
yaxis = 'log luminosity / Lsun'
xlabel_html = r'log<sub>10</sub>(<i>t</i><sub>plateau</sub>/d)'
subplots = True
xrange = [0,4]
rebin = None

plots = {
    'main' :
    {
        'name' : 'transients',
        'xcol' : xcol,
        'xlabel_html' : xlabel_html,
        'ycol' : yaxis,
        'ylabel_html' : ensemble_bokeh.yaxis_HTML_label(yaxis),
        'zcol' : 'probability',
        'title' : plot_title,
        'font' : 'Helvetica',
        'width' : 800,
        'height' : 800,
        'xaxis' : {
            'axis_label' : ensemble_bokeh.xaxis_label(xcol),
        },
        'yaxis' :{
            'axis_label' : ensemble_bokeh.yaxis_label(yaxis),
        },
        'x_range' : [0,5],
        'y_range' : [3,8],
        'x_axis_location' : "below",
        'x_range_flipped' : False,
        'y_range_flipped' : False,
        'logzdata' : True,
        'tooltips' : ensemble_bokeh.tooltips_string(xcol,yaxis),
        'output_mode' : 'inline', # 'inline' (all local, larger file) or 'cdn' (loads javascript from remote source) or 'local' (use local copy of cdn files)

        'subplots' : subplots,
        'limit_range_x' : xrange,

        # data massage
        'rebin' : rebin,
        'zdex' : 6, # show only top 6 dex (requires logzdata)
    },
    'dist:m1 pre' :
    {
        'name' : 'dist:m1 pre',
        'dataname' : 'm1dist pre',
        'title' : 'log(Primary mass) distribution pre-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (-2,+2.3),
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(M_1/\mathrm{M}_\odot)$$',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}log_{10}(M_1/\mathrm{M}_\odot)$$',
        },
        'tooltips' : None,
        'dx' : 0.1,
        'disttype' : 'float',
        'stellar type extension' : True
    },
    'dist:m1 post' :
    {
        'name' : 'dist:m1 post',
        'dataname' : 'm1dist post',
        'title' : 'log(Primary mass) distribution post-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (-2,+2.3),
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(M_1/\mathrm{M}_\odot)$$',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}log_{10}(M_1/\mathrm{M}_\odot)$$',
        },
        'tooltips' : None,
        'dx' : 0.1,
        'disttype' : 'float',
    },
    'dist:m2 pre' :
    {
        'name' : 'dist:m2 pre',
        'dataname' : 'm2dist pre',
        'title' : 'log(Secondary mass) distribution pre-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (-2,+2.3),
        #'y_range' : (0,2),
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(M_2/\mathrm{M}_\odot)$$',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}log_{10}(M_2/\mathrm{M}_\odot)$$',
        },
        'tooltips' : None,
        'dx' : 0.1,
        'disttype' : 'float',
    },
    'dist:m2 post' :
    {
        'name' : 'dist:m2 post',
        'dataname' : 'm2dist post',
        'title' : 'log(Secondary mass) distribution post-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (-2,+2.3),
        #'y_range' : (0,2),
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(M_2/\mathrm{M}_\odot)$$',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}log_{10}(M_2/\mathrm{M}_\odot)$$',
        },
        'tooltips' : None,
        'dx' : 0.1,
        'disttype' : 'float',
    },
    'dist:p pre' :
    {
        'name' : 'dist:p pre',
        'dataname' : 'pdist pre',
        'title' : 'log(Orbital period/days) distribution pre-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (math.log10(1e-3),math.log10(1e9)),
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(P_\mathrm{orb}/\mathrm{d})$$',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}\log_{10}(P_\mathrm{orb}/\mathrm{d})$$',
        },
        'tooltips' : None,
        'dx' : 0.1,
        'rebinx' : 0.5,
        'disttype' : 'float',
    },
    'dist:p post' :
    {
        'name' : 'dist:p post',
        'dataname' : 'pdist post',
        'title' : 'log(Orbital period/days) distribution post-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (math.log10(1e-3),math.log10(1e9)),
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(P_\mathrm{orb}/\mathrm{d})$$',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}\log_{10}(P_\mathrm{orb}/\mathrm{d})$$',
        },
        'tooltips' : None,
        'dx' : 0.1,
        'rebinx' : 0.5,
        'disttype' : 'float',
    },
    'dist:e pre' :
    {
        'name' : 'dist:e pre',
        'dataname' : 'edist pre',
        'title' : 'Eccentricity distribution pre-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (0,1),
        'xaxis' : {
            'axis_label' : r'Eccentricity',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}e$$',
        },
        'tooltips' : None,
        'dx' : 0.05,
        'disttype' : 'float',
    },
    'dist:e post' :
    {
        'name' : 'dist:e post',
        'dataname' : 'edist post',
        'title' : 'Eccentricity distribution post-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (0,1),
        'xaxis' : {
            'axis_label' : r'Eccentricity',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}e$$',
        },
        'tooltips' : None,
        'dx' : 0.05,
        'disttype' : 'float',
    },
    'dist:st1 pre' :
    {
        'name' : 'dist:st1 pre',
        'dataname' : 'st1dist pre',
        'title' : 'Primary stellar type distribution pre-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (0,15),
        'tooltips' : None,
        'dx' : 1,
        'disttype' : 'int',
        'xaxis': {
            'axis_label' : r'Stellar type',
            'major_label_overrides' : ensemble_bokeh.stellar_type_label_dict(),
            'major_label_orientation' : 'vertical',
            'major_label_text_font_size' : '10px',
            'major_label_policy' : bokeh.models.AllLabels(),
            'ticker' : bokeh.models.FixedTicker(ticks=list(range(0,16)))
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d\,(Stellar\,type)}$$',
        },
    },
    'dist:st1 post' :
    {
        'name' : 'dist:st1 post',
        'dataname' : 'st1dist post',
        'title' : 'Primary stellar type distribution post-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (0,15),
        'tooltips' : None,
        'dx' : 1,
        'disttype' : 'int',
        'xaxis': {
            'axis_label' : r'Stellar type',
            'major_label_overrides' : ensemble_bokeh.stellar_type_label_dict(),
            'major_label_orientation' : 'vertical',
            'major_label_text_font_size' : '10px',
            'major_label_policy' : bokeh.models.AllLabels(),
            'ticker' : bokeh.models.FixedTicker(ticks=list(range(0,16)))
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d\,(Stellar\,type)}$$',
        },
    },
    'dist:st2 pre' :
    {
        'name' : 'dist:st2 pre',
        'dataname' : 'st2dist pre',
        'title' : 'Secondary Stellar type distribution pre-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (0,15),
        'tooltips' : None,
        'dx' : 1,
        'disttype' : 'int',
        'xaxis' : {
            'major_label_overrides' : ensemble_bokeh.stellar_type_label_dict(),
            'major_label_orientation' : 'vertical',
            'major_label_text_font_size' : '10px',
            'major_label_policy' : bokeh.models.AllLabels(),
            'ticker' : bokeh.models.FixedTicker(ticks=list(range(0,16))),
            'axis_label' : r'Stellar type',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d\,(Stellar\,type)}$$',
        },
    },
    'dist:st2 post' :
    {
        'name' : 'dist:st2 post',
        'dataname' : 'st2dist post',
        'title' : 'Secondary Stellar type distribution post-nova',
        'width' : 400,
        'height' : 400,
        'x_range' : (0,15),
        'tooltips' : None,
        'dx' : 1,
        'disttype' : 'int',
        'xaxis' : {
            'major_label_overrides' : ensemble_bokeh.stellar_type_label_dict(),
            'major_label_orientation' : 'vertical',
            'major_label_text_font_size' : '10px',
            'major_label_policy' : bokeh.models.AllLabels(),
            'ticker' : bokeh.models.FixedTicker(ticks=list(range(0,16))),
            'axis_label' : r'Stellar type',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d\,(Stellar\,type)}$$',
        },
    },
    'map:logP-e pre' :
    {
        'name' : 'map:logP-e pre',
        'dataname' : 'P-e map pre',
        'title' : r'$$\log_{10}(P_\mathrm{orb})\,-\,e\,-\,\log_{10}(N_\mathrm{stars})~\mathrm{pre-nova}$$',
        'width' : 400,
        'height' : 400,
        'x_range' : (-1,9), # plot range
        'y_range' : (0,1), # plot range
        'limit_range_x' : (-1,9), # data limiter
        'limit_range_y' : (0,1), # data limiter
        'limit_range_z' : (-4,0), # data limiter (applied after logz)
        'normalize_to_zmax' : True, # divide by zmax (applied before logz)
        'logz' : True, # plot z on log scale
        'tooltips' : None,
        'disttype' : 'float',
        'dx' : None,
        'dy' : None,
        'ncolour_bar' : 100,
        'colour_bar_index' : 0.5,
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(P_\mathrm{orb}/\mathrm{d})$$',
            },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{Eccentricity}$$',
            },
        'roundfunc' : ensemble_bokeh.round2,
        'rebinx' : 0.5,
        'rebiny' : 0.1,
    },
    'map:logP-e post' :
    {
        'name' : 'map:logP-e post',
        'dataname' : 'P-e map post',
        'title' : r'$$\log_{10}(P_\mathrm{orb})\,-\,e\,-\,\log_{10}(N_\mathrm{stars})~\mathrm{post-nova}$$',
        'width' : 400,
        'height' : 400,
        'x_range' : (-1,9), # plot range
        'y_range' : (0,1), # plot range
        'limit_range_x' : (-1,9), # data limiter
        'limit_range_y' : (0,1), # data limiter
        'limit_range_z' : (-4,0), # data limiter (applied after logz)
        'normalize_to_zmax' : True, # divide by zmax (applied before logz)
        'logz' : True, # plot z on log scale
        'tooltips' : None,
        'disttype' : 'float',
        'dx' : None,
        'dy' : None,
        'ncolour_bar' : 100,
        'colour_bar_index' : 0.5,
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(P_\mathrm{orb}/\mathrm{d})$$',
            },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{Eccentricity}$$',
            },
        'roundfunc' : ensemble_bokeh.round2,
        'rebinx' : 0.5,
        'rebiny' : 0.1,
    }
}

############################################################
# make and save list of subplots
############################################################
if plots['main'].get('subplots',True):
    plots['main']['subplots'] = list(k for k in plots.keys() if k != 'main')
else:
    plots['main']['subplots'] = []

all_types_data = all_times_data['stellar type primary']['all']['stellar type secondary']['all']
by_stellar_type_data = {}
if plots['main'].get('rebin',False):
    # rebin data?
    if vb:
        print("Rebin ensemble")
    r = plots['main']['rebin']
    all_types_data = ensemble_bokeh.rebin_4D(plots=plots,
                                             data=all_types_data,
                                             dx=r[0],
                                             dy=r[1],
                                             vb=vb)


for stellar_type1 in all_times_data['stellar type primary']:
    by_stellar_type_data[stellar_type1] = {}
    for stellar_type2 in all_times_data['stellar type primary'][stellar_type1]['stellar type secondary']:
        by_stellar_type_data[stellar_type1] = \
            binarycpython.utils.dicts.merge_dicts(by_stellar_type_data[stellar_type1],
                                                  all_times_data['stellar type primary'][stellar_type1]['stellar type secondary'][stellar_type2]);



if vb:
    print("Analyse ensemble data")
analysis = ensemble_bokeh.analyse_4D(all_types_data,vb=vb)

try:
    if vb:
        print("Map 4D to CDS")

    (datasource,zstats) = ensemble_bokeh.map_4D_to_ColumnDataSource(
        all_types_data,
        plots = plots,
        analysis = analysis,
        vb = False,
        by_stellar_type_data = by_stellar_type_data
    )
except Exception as e:
    print("\nFailed to load datasource")
    s,r = getattr(e, 'message', str(e)), getattr(e, 'message', repr(e))
    print ('s:', s, 'len(s):', len(s))
    print ('r:', r, 'len(r):', len(r))
    print(traceback.format_exc())
    exit()

if vb:
    print("Make figures")
# make figures and renderers dict
figures = ensemble_bokeh.make_bokeh_figures(plots)

############################################################
# make large diagram in the main plot
#
# first, make a colour bar and add it
if vb:
    print("Make colourbar")
colour_bars = ensemble_bokeh.bokeh_colourbar(zstats = zstats)
for c in colour_bars:
    figures[dist].add_layout(c,'right')

if vb:
    print("Add " + dist)
# secondly, add the data as a map ("rect" type)
renderers = { dist : figures[dist].rect(
    x = plots['main']['xcol'], # x column
    y = plots['main']['ycol'], # y column
    fill_color = { # fill with colour_mapper (see above)
        'field' : 'hex_colours',
    },
    # "pixel" size in the distribution : you might want to increase these?
    width = analysis['dx'],
    height = analysis['dy'][plots['main']['ycol']],
    width_units = 'data',
    height_units = 'data',
    source = datasource, # data source
    line_color = None,
    dilate = True,
    name = dist,
)}

############################################################
# make the subplots and link their data to the main plot
if plots['main'].get('subplots',True):
    if vb:
        print("Make subplots")
    (renderers,linkdatas,subplot_colour_bars) = ensemble_bokeh.make_subplots(
        all_types_data,
        plots=plots,
        figures=figures,
        renderers=renderers,
        datasource=datasource
    )
else:
    renderers = []
    subplot_colour_bars = None

figures[dist].legend.location='bottom_left'
############################################################
# make custom javascript to update the sub plot
# when hovering over the main plot
#
# Here we assume one renderer is the main (here, dist)
# and the others are subordinates
if plots['main'].get('subplots',True):
    if vb:
        print("Link plots")
    ensemble_bokeh.link_plots(
        main_figure = figures[dist],
        main_renderer = dist,
        renderers = renderers,
        linkdatas = linkdatas
        )

# output HTML
if vb:
    print(f"Write to {dist}.html")
ensemble_bokeh.output_plot(
    dist = dist,
    figures = figures,
    renderers = renderers,
    subplot_colour_bars = subplot_colour_bars,
    filename = f'{dist}.html',
    title = 'binary_c ensemble',
    plots = plots,
    allow_CORS = True, # might be insecure!
    scrollbars = True,
    zstats = zstats
)
