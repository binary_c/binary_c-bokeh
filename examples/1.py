#!/usr/bin/env python3
from bokeh.plotting import figure,show

# data for the chart

x = [1,2,3,4,5]
y = [3,5,2,6,6]

p = figure(title="The title of the graph",
           x_axis_label='xxx',
           y_axis_label='yyy')
p.line(x,
       y,
       legend_label="my data",
       line_width=3)

show(p)
