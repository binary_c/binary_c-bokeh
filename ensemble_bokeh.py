############################################################
#
# Functions used in Bokeh plots of binary_c ensembles
#
# see Bokeh_HRD.py for an example of use
#
# Todo :
# auto binwidths in map subplots
#
############################################################

import astropy
import binarycpython
import bokeh
from bokeh.plotting import figure
from bokeh.transform import transform
import collections
import colormap
import math
import random
import re
import numpy as np
import traceback

testing = True
import sys
import json
from bokeh.palettes import Viridis6

G = astropy.constants.G.cgs.to_value() # 6.67408e-08 # G (cgs)
Msun = astropy.constants.M_sun.cgs.to_value() # 1.989e33 # Msun (cgs)
Rsun = astropy.constants.R_sun.cgs.to_value() # 6.9566e+10 # Rsun (cgs)
Lsun = astropy.constants.L_sun.cgs.to_value() # 3.8515e+33 # Lsun (cgs)
sigma = astropy.constants.sigma_sb.cgs.to_value()  # 5.670352798e-5 # Stefan-Boltzmann constant (cgs)
kSB = 4.0 * math.pi * sigma # 4pi * Stefan-Boltzmann - used often
binary_c_log_period_minimum = -49.0 # see binary_fraction()
palette_steps = {'probability': 100,
                 'binfrac' : 20}

stellar_types = range(16)
stellar_type_colours = [
    '#aaff00', # LMMS
    '#dddd00', # MS
    '#ff9900', # HG
    '#ff0000', # GB
    '#0000ff', # CHeB
    '#cc6600', # EAGB
    '#900000', # TPAGB
    '#ff00ff', # HeMS
    '#aa00ff', # HeHG
    '#aa00aa', # HeGB
    '#009090', # HeWD
    '#00ffff', # COWD
    '#0090ff', # ONeWD
    '#aaaaaa', # NS
    '#555555', # BH
    '#000000', # massless
]
def float_powermax(x,k=None):
    """
     if the exponent of x exceeds k, return in exponential
     form, like sprintf's %e, otherwise format as %g float

    Input:
    * x : float
    * k : int or float

    Output:
    * formatted float
    """
    if k != None and \
       math.fabs(math.log10(math.fabs(x)))>=k:
        x = f"{x:e}"
    else:
        x = f"{x:g}"
    return x

def HTML_format_float(x,powermax=None):
    """
    Given a float, x, format it appropriately for HTML.

    Input:
    * x : float
    * powermax : sent to float_powermax if not None

    Output:
    * HTML formatted-float string
    """
    if x == 1.0:
        return "1"
    elif x == 0.0:
        return "0"
    else:
        if powermax != None:
            x = float_powermax(x,powermax)
        return re.sub(r"e([+-])0*(\d+)",
                      r'×10<sup>\1\2</sup>',
                      f"{x:g}")

def latex_format_float(x,powermax=None):
    """
    Given a float, x, format it appropriately for LaTeX/Mathjax.

    Input:
    * x : float
    * powermax : sent to float_powermax if not None

    Output:
    * HTML formatted-float string
    """
    if x == 1.0:
        return "1"
    elif x == 0.0:
        return "0"
    else:
        if powermax != None:
            x = float_powermax(x,powermax)
        x = re.sub(r"e([+-])0*(\d+)",
                   r'\\times 10^{\1\2}',
                   x)
        return x

def binary_fraction(pdist,
                    roundfunc=None):
    """
    Given a period distribution, pdist, compute the binary fraction.
    We can do this because any log10(periods) shorter than
    binary_c_log_period_minimum are single stars, the rest are binary
    stars.

    Input:
    * pdist: dict with log10(orbital period/days) as keys, number of stars as values
    * roundfunc : rounding function (not used if None)

    Output:
    * binary fraction (float)

    if pdist is None, returns 0.0
    """
    if pdist:
        single = binary = 0.0

        for logp in pdist:
            if logp < binary_c_log_period_minimum:
                # single star
                single += pdist[logp]
            else:
                # binary star
                binary += pdist[logp]

        total = single + binary
        if total == 0.0:
            binfrac = 0.0
        else:
            binfrac = binary / total
            if roundfunc:
                binfrac = roundfunc(binfrac)
    else:
        binfrac = 0.0

    return binfrac


def round(x):
    """
    Trim a number, x, like %g in sprintf and return it as a float.

    Input:
    * x : float

    Output:
    * trimmed float
    """
    if x == None:
        return None
    else:
        return float(f"{x:g}")

# alternative rounding functions
def round4(x):
    if x == None:
        return None
    else:
        return float(f"{x:.4g}")

def round3(x):
    if x == None:
        return None
    else:
        return float(f"{x:.3g}")

def round2(x):
    if x == None:
        return None
    else:
        return float(f"{x:.2g}")

def bin_data(x,w):
    """
    Function to bin data, x, to nearest width, w.

    Input:
    * x : scalar datum, float
    * w : bin width, float

    Output:
    * binned float
    """
    return round((int(x/w) + 0.5*math.copysign(1.0,x))*w)

def dict_2D_to_xy(d,
                  parent=None,
                  minval=None,
                  maxval=None,
                  step=None,
                  disttype='float',
                  rebinx=None,
                  limit_range_x=None,
                  roundfunc=round):
    """
    Convert 2D dict of nested data { ... : {... } } to x,y lists
    { 'x' : [...], 'y' : [...] }
    as required for Bokeh plotting.
    """
    output = { 'x' : [], 'y' : [] }
    if d != None:
        if minval != None and maxval != None and step != None:
            # loop and fill in blanks
            if disttype == 'float':
                x = roundfunc(minval + 0.5*step)
            else:
                x = minval

            if rebinx == None:
                # no rebin
                while x<maxval:
                    if in_range(x, limit_range_x):
                        output['x'].append(x)
                        output['y'].append(d.get(x,0.0))
                    x += step
                    if disttype == 'float':
                        x = roundfunc(x)
            else:
                # rebin x values
                newd = {}
                for x in d.keys():
                    newx = roundfunc(bin_data(x,rebinx))
                    newy = d.get(x,0.0)
                    if newx in newd:
                        newd[newx] += newy
                    else:
                        newd[newx] = newy
                d = newd

                # get the new list of keys, hence the min, max
                x = roundfunc(bin_data(minval,rebinx))
                maxkey = roundfunc(bin_data(maxval,rebinx))
                while x < maxkey:
                    if in_range(x, limit_range_x):
                        output['x'].append(x)
                        output['y'].append(newd.get(x,0.0))
                    x += rebinx
                    if disttype == 'float':
                        x = roundfunc(x)

        else:
            # use keys directly
            for x in sorted(d.keys()):
                if in_range(x, limit_range_x):
                    output['x'].append(x)
                    output['y'].append(d[x])

    return output

def rebin_4D(plots=None,
             data=None,
             dx=None,
             dy=None,
             vb=False):
    """
Rebin ensemble data in 4D nested dict form, where (x,y)
are the main variables (e.g. logTeff and logL) to new resolution,
(dx,dy), bins.

Input:
    * data : 4D ensemble data
{ 'xkey' : { ...xvalues... : {'ykey1' : { ... }, 'ykey2' : { ... } , ... } } }

    * dx,dy : new resolution of x,y
    * plots : list of plots (see Bokeh_HRD.py)
    * vb : verbosity

Output:
    * returns 4D ensemble data but rebinned appropriately.
    """
    newdata = {}
    for xaxis in data:
        newdata[xaxis] = {}
        for x in data[xaxis]:
            newx = bin_data(x,dx)
            if not newx in newdata[xaxis]:
                newdata[xaxis][newx] = {}
            for yaxis in data[xaxis][x]:
                if not yaxis in newdata[xaxis][newx]:
                    newdata[xaxis][newx][yaxis] = {}
                for y in data[xaxis][x][yaxis]:
                    newy = bin_data(y,dy)
                    if not newy in newdata[xaxis][newx][yaxis]:
                        newdata[xaxis][newx][yaxis][newy] = {}
                    try:
                        newdata[xaxis][newx][yaxis][newy] = \
                            binarycpython.utils.dicts.merge_dicts(
                                data[xaxis][x][yaxis][y],
                                newdata[xaxis][newx][yaxis][newy]
                            )

                    except Exception as e:
                        print("\nFailed to load datasource")
                        s,r = getattr(e, 'message', str(e)), getattr(e, 'message', repr(e))
                        print ('s:', s, 'len(s):', len(s))
                        print ('r:', r, 'len(r):', len(r))
                        print(traceback.format_exc())
                        exit()


    return newdata

def in_range(x,
             range_tuplet):
    """
    Check if x is in the range of the two numbers in range_tuplet.

    If range_tuplet is None or x is None, return True.

    If either element of range_tuplet is none, the element is ignored.
    """
    return range_tuplet == None or x == None or \
        ((range_tuplet[0] == None or x >= range_tuplet[0]) and \
         (range_tuplet[1] == None or x <= range_tuplet[1]))

def dict_3D_to_xyz(d,
                   normalize=True,
                   parent=None,
                   limit_range_x=None,
                   limit_range_y=None,
                   limit_range_z=None,
                   rebinx=None,
                   rebiny=None,
                   normalize_to_zmax=True,
                   logz=False,
                   roundfunc=round,
                   zmin=None):
    """
    Convert 3D dict of nested data { ... : { ... : { ... : ... } } } to x,y,z lists
    { 'x' : [...], 'y' : [...], 'z' : [...] }
    as required for Bokeh plotting.

    Dy default, normalize the z coordinate.
    """
    output = { 'x' : [], 'y' : [], 'z' : [] }

    if d != None:
        # find zmax
        zmax = 0.0
        xkeys = sorted(d.keys())
        for x in xkeys:
            if in_range(x, limit_range_x):
                for y in d[x].keys():
                    if in_range(y, limit_range_y):
                        zmax = max(zmax, d[x][y])

        # rebin x
        if rebinx is not None:
            newd = {}
            for x in d.keys():
                newx = roundfunc(bin_data(x,rebinx))
                if newx in newd:
                    newd[newx] = dict(binarycpython.utils.dicts.merge_dicts(
                        newd[newx],
                        d[x]
                        ))
                else:
                    newd[newx] = d[x]
            d = newd
            xkeys = sorted(d.keys())

        # rebin y
        if rebiny is not None:
            newd = {}
            for x in d.keys():
                for y in d[x].keys():
                    newy = roundfunc(bin_data(y,rebiny))
                    if not x in newd:
                        newd[x] = {}
                    if newy in newd[x]:
                        newd[x][newy] += d[x][y]
                    else:
                        newd[x][newy] = d[x][y]
            d = newd

        # set data and normalize if required
        for x in xkeys:
            if in_range(x, limit_range_x):
                for y in sorted(d[x].keys()):
                    if in_range(y, limit_range_y):
                        z = d[x][y]
                        if logz:
                            if z > 0.0:
                                if normalize_to_zmax and zmax > 0.0:
                                    z /= zmax
                                z = math.log10(z)
                                if in_range(z, limit_range_z):
                                    output['x'].append(x)
                                    output['y'].append(y)
                                    output['z'].append(z)
                        else:
                            if normalized_to_zmax and zmax > 0.0:
                                z /= zmax
                            if in_range(z, limit_range_z):
                                output['x'].append(x)
                                output['y'].append(y)
                                output['z'].append(z)

    return output


def map_4D_to_ColumnDataSource(d,
                               plots=None,
                               roundfunc=round,
                               analysis=None,
                               nodata=math.nan,
                               vb=False,
                               by_stellar_type_data=None):
    """
Map a 4D dict from an ensemble to a 2D dict for Bokeh plotting.

This function fills the data space. Where there is no data available
nodata is used, so this is normally math.nan to prevent plotting.

    Input:
* d : 4D ensemble dictionary
* plots : list of plots (see Bokeh-HRD.py)
* roundfunc : function to round floating point data (usually round())
* analysis : data analysis dict. Generated automatically if None.
* nodata : value used to fill empty space, usually math.nan to avoid plotting.
* vb : verbosity

    Output:
* Bokeh ColumnDataSource for making the plots.
    """

    # get main plot, axes and options
    m = plots['main']
    logz = m.get('logzdata',False)
    special_axes = ('binfrac','formatted_probability','hex_colours')
    axes = ( m['xcol'], m['ycol'], m['zcol'] ) + tuple(m['subplots'])

    (xaxis,yaxis,zaxis,*linktypes) = axes
    limit_range_x = plots['main'].get('limit_range_x',None)

    # add special (non-subplot) axes
    axes += tuple(special_axes)

    # analyse data if we haven't already
    if analysis == None:
        if(vb):
            print("Analyse data (analysis passed in was empty)")
        analysis = analyse_4D(d,roundfunc=round)
    def __do_nothing(x):
        return x
    if not roundfunc:
        roundfunc = __do_nothing
    if not xaxis or not yaxis:
        print(f"First two args should be the x and y axis labels")
    if not yaxis in analysis['yaxes']:
        print(f"Requested yaxis = {yaxis} is not in 4D dict's yaxes = {analysis['yaxes']}")
        exit()

    # set up data dict
    data = {}
    for axis in axes:
        data[axis] = []

    zmax = None
    zmin = None
    if logz and 'zdex' in m and m['zdex'] != None:
        if(vb):
            print("First parse to find zmin,zmax for zdex limit")
        # we want to limit to the top zdex dex of
        # probabilities : need to loop to find the
        # lower limit, zmax
        x = roundfunc(analysis['xmin'])
        while x <= analysis['xmax']:
            y = roundfunc(analysis['ymin'][yaxis])
            while y <= analysis['ymax'][yaxis]:
                try:
                    z = d[xaxis][x][yaxis][y]['value']
                    if zmax == None:
                        zmax = z
                    else:
                        zmax = max(zmax,z)
                except:
                    zdata = None
                y = roundfunc(y + analysis['dy'][yaxis])
            x = roundfunc(x + analysis['dx'])
        if zmax != None:
            zmax = math.log10(zmax)
            zmin = zmax - m['zdex']
        else:
            print("No zmax found in the data : please check the x and y columns are correct, and that there are data corresponding to them.")
            sys.exit()

    # x loop (usually logTeff)
    x = roundfunc(analysis['xmin'])
    while x<=analysis['xmax']:
        if vb:
            progress = 100.0*(x-analysis['xmin'])/analysis['xdelta']
            print(f"4D->2D x={x} -> {progress:5.2f} %\r",
                  end="")

        # y loop (usually logL)
        y = roundfunc(analysis['ymin'][yaxis])
        while y<=analysis['ymax'][yaxis]:

            # check that we have data: that we don't
            # just means this is an empty part of the HRD
            try:
                zdata = d[xaxis][x][yaxis][y]
                # the probability goes in z
                if logz:
                    z = math.log10(zdata['value'])
                else:
                    z = zdata(zdata['value'])
            except:
                zdata = None
                z = None

            if zdata != None and in_range(z,(zmin,zmax)):
                # add linktype variables to linkdata
                linkdata = []

                #### TODO:
                # linkdata is a list of linked data, but should be a dict
                # because some are pre, some are post, or neither

                if linktypes:
                    for linktype in linktypes:
                        plot = plots[linktype]

                        # first try the dataname as the key to access the data
                        this_data = zdata.get(plots[linktype]['dataname'],None)
                        if this_data is None:
                            # this failed, try the linktype
                            this_data = zdata.get(linktype,None)

                        if linktype.startswith('map:'):
                            # 3D distribution
                            xyz_data = dict_3D_to_xyz(
                                this_data,
                                normalize=True,
                                limit_range_x = plot.get('limit_range_x',None),
                                limit_range_y = plot.get('limit_range_y',None),
                                parent=(x,y),
                                logz=plot.get('logz',False),
                                zmin=plot.get('zmin',None),
                                rebinx=plot.get('rebinx',None),
                                rebiny=plot.get('rebiny',None),
                                roundfunc=plot.get('roundfunc',None)
                            )
                            if xyz_data == None:
                                print(f"ERROR: xy data {linktype} none")
                            linkdata.append(xyz_data)
                        elif linktype.startswith('dist:'):
                            # 2D distribution
                            xy_data = dict_2D_to_xy(
                                this_data,
                                minval = plot['x_range'][0],
                                maxval = plot['x_range'][1],
                                step = plot['dx'],
                                disttype = plot['disttype'],
                                rebinx = plot.get('rebinx',None),
                                limit_range_x = plot.get('limit_range_x',None),
                                parent=(x,y)
                            )
                            if xy_data == None:
                                print(f"ERROR: xy data {linktype} none")
                                sys.exit()

                            # convert to multiline format
                            colour = 'black'
                            xy_data = {
                                'xs' : [xy_data['x']],
                                'ys' : [xy_data['y']],
                                'colour' : [colour],
                                'label' : ['all'],
                                'width' : [4],
                            }

                            # append subtypes
                            #
                            # use stellar type colours (defined at the top)
                            colours = stellar_type_colours
                            stellar_types = stellar_type_label_dict()
                            stellar_type1 = 0
                            for stellar_type in stellar_type_label_dict():
                                if stellar_type < 15: # do not plot massless remnants
                                    colour = colours[stellar_type1%len(colours)]

                                    try:
                                        sub_data = by_stellar_type_data[stellar_type1][m['xcol']][x][m['ycol']][y][plots[linktype]['dataname']]
                                    except:
                                        sub_data = None

                                    if sub_data:
                                        sub_xy_data = dict_2D_to_xy(
                                            sub_data,
                                            minval = plot['x_range'][0],
                                            maxval = plot['x_range'][1],
                                            step = plot['dx'],
                                            disttype = plot['disttype'],
                                            rebinx = plot.get('rebinx',None),
                                            limit_range_x = plot.get('limit_range_x',None),
                                            parent=(x,y)
                                        )
                                    else:
                                        sub_xy_data = {'x':[],'y':[]}

                                    if 'x' in sub_xy_data:
                                        xy_data['xs'].append(sub_xy_data['x'])
                                        xy_data['ys'].append(sub_xy_data['y'])
                                    else:
                                        # dummy data (empty lists)
                                        xy_data['xs'].append([nan])
                                        xy_data['ys'].append([nan])
                                    xy_data['colour'].append(colour)
                                    xy_data['label'].append(stellar_types[stellar_type1])
                                    xy_data['width'].append(2)
                                    stellar_type1 += 1

                            # finally, add data to the link data
                            linkdata.append(xy_data)
                        else:
                            print(f'error: distribution type {linetype} is unknown')
                            sys.exit(1)

                # special case: binary fraction
                binary_fraction_data = zdata.get('pdist',None)
                if binary_fraction_data == None:
                    binary_fraction_data = zdata.get('pdist post',None)
                linkdata.append(binary_fraction(binary_fraction_data,
                                                roundfunc=roundfunc))

                # special case: probability for formatted_probability strings
                linkdata.append(z)

                # append data to the lists for Bokeh
                for (axis,datum) in zip(axes,(x,y,z) + tuple(linkdata)):
                    data[axis].append(datum)

            # increment y
            y = roundfunc(y + analysis['dy'][yaxis])

        # increment x
        x = roundfunc(x + analysis['dx'])

    if(vb):
        print()

    # set z (probability and binfrac) stats
    zstats = {
        'x' : { # rescaled probability, ranges from 0 to 1
            'low' : min(data['probability']),
            'high' : max(data['probability'])
        },
        'y' : { # binary fraction, must be between 0 and 1
            'low' : 0.0,
            'high' : 1.0
        }
    }
    for axis in ('x','y'):
        zstats[axis]['delta'] = zstats[axis]['high'] - zstats[axis]['low']

    # save a formatted probability string for tooltips
    formatted_probabilities = []
    for x in data['probability']:
        formatted_probabilities.append(HTML_format_float(x))
    data['formatted_probability'] = formatted_probabilities

    # adjust z values for 2D probability-binfrac colouring
    hex_colours = []
    for (probability,binfrac) in zip(data['probability'],data['binfrac']):
        hex_colour = map_probability_binfrac_to_hex(probability,
                                                    binfrac,
                                                    zstats)
        hex_colours.append(hex_colour)

    data['hex_colours'] = hex_colours

    # reformat binfracs to HTML strings for tooltips
    new_binfracs = []
    for binfrac in data['binfrac']:
        new_binfracs.append(HTML_format_float(binfrac))
    data['binfrac'] = new_binfracs

    return (bokeh.models.ColumnDataSource(data=data),zstats)

def analyse_2D(d,s=None,roundfunc=round,vb=False):
    """
Analyse a 2D dict, d, and return a dict, s, of statistics.

See analyse_4D for details, although note that in 2D
we do not know the axis information.
    """
    # set up s dict, which is returned
    if s == None or not s:
        s = {
            'xmin' : 1e200,
            'xmax' : -1e200,
            'ymin' : 1e200,
            'ymax' : -1e200,
            'zmin' : 1e200,
            'zmax' : -1e200,
            'xdelta' : None,
            'ydelta' : None,
            'xkeys_dict' : {},
            'ykeys_dict' : {},
            'xkeys' : [],
            'ykeys' : [],
            'dy' : None,
            'dx' : None
        }
    prevx = None
    __x = sorted(list(d))
    for x in sorted(d):
        s['xmin'] = min(s['xmin'],x)
        s['xmax'] = max(s['xmax'],x)
        s['xkeys_dict'][x] = 1
        if prevx != None:
            dx = roundfunc(x - prevx)

            if dx < 0.0:
                print(f"dx = {dx} < 0 : x = {x}, prevx = {prevx}, error")
                exit()
            if s['dx'] == None:
                s['dx'] = dx
            else:
                s['dx'] = min(dx,s['dx'])
            #print(f"2D: dy {x} - {prevx} = {dx} -> {s['dx']}")
        prevx = x

        prevy = None
        for y in sorted(d[x].keys()):
            z = d[x][y]
            s['ykeys_dict'][y] = 1
            s['ymin'] = min(s['ymin'],y)
            s['ymax'] = max(s['ymax'],y)
            if prevy != None:
                dy = roundfunc(y - prevy)
                if s['dy'] == None:
                    s['dy'] = dy
                else:
                    s['dy'] = min(s['dy'],dy)
                #print(f"2D: dy {y} - {prevy} = {dy} -> {s['dy']}")
            s['zmin'] = min(s['zmin'],z)
            s['zmax'] = max(s['zmax'],z)
            prevy = y

    s['xkeys'] = sorted(s['xkeys_dict'])
    s['ykeys'] = sorted(s['ykeys_dict'].keys())

    # round everything
    if roundfunc:
        s['dx'] = roundfunc(s['dx'])
        s['dy'] = roundfunc(s['dy'])
        s['xmin'] = roundfunc(s['xmin'])
        s['xmax'] = roundfunc(s['xmax'])
        s['ymin'] = roundfunc(s['ymin'])
        s['ymax'] = roundfunc(s['ymax'])

    s['xdelta'] = s['xmax'] - s['xmin']
    s['ydelta'] = s['ymax'] - s['ymin']

    if roundfunc:
        s['xdelta'] = roundfunc(s['xdelta'])
        s['ydelta'] = roundfunc(s['ydelta'])

    return s

def analyse_4D(d,s=None,roundfunc=round,vb=False):
    """
Analyse a 4D ensemble dict, d, and return a dict, s, of statistics.

The ensemble dict is assumed to have one x key but possibly multiple
y keys.
{ 'xkey' : { ...xvalues... : {'ykey1' : { ... }, 'ykey2' : { ... } , ... } } }

The statistics include the min, max, delta (=max-min), bin sizes, axis and key lists.
    """
    # set up s dict, which is returned
    if s == None or not s:
        init = True
        s = {
            'xmin' : 1e200,
            'xmax' : -1e200,
            'xdelta' : None,
            'ydelta' : {},
            'xkeys_dict' : {},
            'ykeys_dict' : {},
            'xkeys' : [],
            'ykeys' : {}, # one for each yaxis
            'xaxes' : [],
            'yaxes' : [],
            'ymin' : {},
            'ymax' : {},
            'dx' : None,
            'dy' : {},
        }
    else:
        init = False
    prevx = None
    xaxes = sorted(d.keys())
    yaxes = {}
    for xaxis in xaxes:
        for x in d[xaxis]:
            for yaxis in d[xaxis][x]:
                yaxes[yaxis] = 1
    yaxes = sorted(yaxes.keys())
    for yaxis in yaxes:
        if not yaxis in s['ymin']:
            s['ymin'][yaxis] = 1e200
        if not yaxis in s['ymax']:
            s['ymax'][yaxis] = -1e200
        if not yaxis in s['ykeys']:
            s['ykeys_dict'][yaxis] = {}

    for xaxis in d:
        __x = sorted(list(d[xaxis]))
        prevx = None
        for x in sorted(d[xaxis]): # MUST be sorted for CMDs
            s['xmin'] = min(s['xmin'],x)
            s['xmax'] = max(s['xmax'],x)
            s['xkeys_dict'][x] = 1
            if prevx != None:
                dx = roundfunc(x - prevx)
                if dx < 0.0:
                    print(f"dx = {dx} < 0 : x = {x}, prevx = {prevx}, error")
                    exit()
                if s['dx'] == None:
                    s['dx'] = dx
                else:
                    s['dx'] = min(dx,s['dx'])
            prevx = x

            if s['ymin'] == None:
                s['ymin'] = {}
                for yaxis in sorted(d[xaxis][x]):
                    if not yaxis in s['ymin']:
                        s['ymin'][yaxis] = 1e200
                    if not yaxis in s['ymax']:
                        s['ymax'][yaxis] = -1e200

            for yaxis in yaxes:
                if yaxis in d[xaxis][x]:
                    prevy = None
                    for y in sorted(d[xaxis][x][yaxis].keys()):
                        s['ykeys_dict'][yaxis][y] = 1
                        s['ymin'][yaxis] = min(s['ymin'][yaxis],y)
                        s['ymax'][yaxis] = max(s['ymax'][yaxis],y)
                        if prevy != None:
                            dy = roundfunc(y - prevy)
                            if not yaxis in s['dy']:
                                s['dy'][yaxis] = dy
                            else:
                                s['dy'][yaxis] = min(s['dy'][yaxis],dy)
                        prevy = y

    s['xaxes'] = list(set(xaxes + s['xaxes']))
    s['yaxes'] = list(set(yaxes + s['yaxes']))
    s['xkeys'] = sorted(s['xkeys_dict'])
    for yaxis in yaxes:
        s['ykeys'][yaxis] = sorted(s['ykeys_dict'][yaxis].keys())

    # round everything
    s['dx'] = roundfunc(s['dx'])
    s['xmin'] = roundfunc(s['xmin'])
    s['xmax'] = roundfunc(s['xmax'])
    for yaxis in yaxes:
        s['dy'][yaxis] = roundfunc(s['dy'][yaxis])
        s['ymin'][yaxis] = roundfunc(s['ymin'][yaxis])
        s['ymax'][yaxis] = roundfunc(s['ymax'][yaxis])

    s['xdelta'] = roundfunc(s['xmax'] - s['xmin'])
    for yaxis in yaxes:
        s['ydelta'][yaxis] = roundfunc(s['ymax'][yaxis] - s['ymin'][yaxis])

    return s

def clip(value, lower=0.0, upper=1.0):

    """
    clip a value to range 0-1 (or custom, if given)
    (like numpy.clip() but faster for scalars)

    input:
    * value : float to be clipped
    * lower : lower possible value (0.0)
    * upper : upper possible value (1.0)

    output:
    * returns clipped float
    """
    return lower if value < lower else upper if value > upper else value

def fxfy(x,y,zstats):
    """
map (x,y) to fractional (fx,fy) given ranges in zstats.

We clip() the results to make sure they are in the range [0,1]

    Input:
* x,y : coordinates (floats)
* zstats : statistics dict containing low,high values of the
           coordinates (set by map_4D_to_ColumnDataSource)

    Output:
* (fx,fy) : fractional coordinates (tuple of two floats)
    """
    fx = clip(( x - zstats['x']['low'] ) / zstats['x']['delta'])
    fy = clip(( y - zstats['y']['low'] ) / zstats['y']['delta'])
    return (fx,fy)

def map_probability_binfrac_to_hex(probability,binfrac,zstats):
    """
Given probability and binfrac, as well as zstats (set by
map_4D_to_ColumnDataSource) containing the min,max of these
distributions, map to a hex colour string, e.g. "#ABCDEF".

See also map_2D_to_rgb for the mapping function.
    """
    (r,g,b) = map_2D_to_rgb(*fxfy(probability,binfrac,zstats))
    return colormap.rgb2hex(int(r),int(g),int(b))

def map_2D_to_rgb(fx,fy):
    """
Given fractional x,y values, fx and fy,
map to a 2D rgb colour that, in one direction (x),
maps blue to red, then in the perpendicular direction
(y) maps from light to dark.

By fractional, I mean : fx = (x-xmin)/(xmax-xmin). See the fxy()
function for details.

    Input:
    * fx,fy : fractional (float) values

    Output:
    * (r,g,b) : tuple of three floats in the range 0-255
                corresponding to the red, green and blue colours.
    """
    z = fx * fy
    r = 255 * (1.0 - z)
    g = 255 * (1.0 - fx)
    b = 255 * (1.0 + z - fx) # = 255 * (1 - (1-fy) * fx)
    return (r,g,b)

def map_2D_to_hex(fx,fy):

    """
As map_2D_to_rgb() but converts the (r,g,b) colour to a hex colour string.

    Input:
    * fx,fy : fractional (float) values

    Output:
    * hex colour string (e.g. "#FF0000")
    """
    (r,g,b) = map_2D_to_rgb(fx,fy)
    return colormap.rgb2hex(int(r),int(g),int(b))

def bokeh_colourbar(zstats=None):

    """
    Make Bokeh colour bar for (probability,binfrac) plot.

    This is a horrible hack that puts many colourbars close
    together to make one colour bar. it works, but won't win
    any efficinecy awards. Bokeh might eventually catch up
    with our needs.

    These colour bars are returned in a list.

    Input:
    * zstats : statistics set by map_4D_to_ColumnDataSource

    Output:
    * list of Bokeh colour bars
    """

    xsteps = palette_steps['probability'] # probability steps
    ysteps = palette_steps['binfrac'] # binfrac steps
    width = 60 # pixel width of the colourbar
    height = 'auto' # pixel height of the colourbar

    dwidth = max(1,int(width/float(ysteps-1)))
    dx = zstats['x']['delta']/float(xsteps-1)

    # make list of 1D mappers
    colour_bars = []
    for i in range(0,ysteps):
        first = i == 0
        last = i == ysteps-1

        y = float(i)/float(ysteps-1)
        fy = clip((y - zstats['y']['low']) / zstats['y']['delta'])

        colours = []
        x = zstats['x']['low']
        while x <= zstats['x']['high']:
            fx = clip((x - zstats['x']['low']) / zstats['x']['delta'])
            colours.append(map_2D_to_hex(fx,fy))
            x += dx
        colour_mapper = bokeh.models.LinearColorMapper(
            palette = colours,
            low = zstats['x']['low'],
            high = zstats['x']['high'],
        )
        colour_bar = bokeh.models.ColorBar(
            color_mapper=colour_mapper,
            location=(0, 0),
            ticker=bokeh.models.BasicTicker(desired_num_ticks=len(colours))
        )

        # remove ticks from all but the last
        colour_bar.margin = -1
        colour_bar.padding = -1
        colour_bar.label_standoff = 0
        colour_bar.width = dwidth
        colour_bar.height = 'auto'
        colour_bar.syncable = False # we don't expect the main plot to change, so don't sync

        if first == True:
            colour_bar.title = 'Single'
            colour_bar.title_standoff = 0
            colour_bar.title_text_align = 'left' # right fails

        if last == True: # NOT elif!
            # last colour bar: want labels with reasonable standoff
            colour_bar.title = 'Binary'
            colour_bar.title_standoff = -35
            colour_bar.label_standoff = 10
            colour_bar.title_text_align = 'left' # right fails
        else:
            # usually, we want no labels or ticks
            colour_bar.major_tick_line_width = 0
            colour_bar.minor_tick_line_width = 0
            # Bokeh < 3
            #colour_bar.formatter = bokeh.models.formatters.FuncTickFormatter(code="""return ""; """)
            # Bokeh >= 3.0
            colour_bar.formatter = bokeh.models.formatters.CustomJSTickFormatter(code="""return ""; """)
        colour_bars.append(colour_bar)

    # return colour bars
    return colour_bars

def make_bokeh_figures(plots=None):

    """
    Make Bokeh figure objects corresponding
    to the plots dict passed in (see Bokeh-HRD.py)

    Input:
    * plots : dict set in Bokeh-HRD.py

    Output
    * figures : dict of Bokeh figures
    """

    figures = collections.OrderedDict()
    plotmap = {
        # Bokeh -> us
#        'plot_width' : 'width', # Bokeh < 3
#        'plot_height' : 'height', # Bokeh < 3
        'width' : 'width', # Bokeh >= 3
        'height' : 'height', # Bokeh >= 3
        'title' : 'title',
        'x_axis_location' : 'x_axis_location',
        'y_axis_location' : 'y_axis_location',
        'tooltips' : 'tooltips',
        'tools' : 'tools',
        'toolbar_location' : 'toolbar_location',
        'name' : 'name',
    }
    # Bokeh defaults
    plotdefaults = {
        #'plot_width' : 800,
        #'plot_height' : 800,
        'width' : 800,
        'height' : 800,
        'title' : "",
        'x_axis_location' : 'below',
        'tooltips' : "",
        'tools' : "pan,wheel_zoom,box_zoom,reset,hover,crosshair",
        'toolbar_location' : 'right',
    }
    for plot,info in plots.items():
        # put as many options as possible into the opts
        # dict to send straight to bokeh's figure()
        if info.get('skip',False) == False:
            opts = {}
            for i in plotdefaults:
                opts[i] = plotdefaults[i]
            for bok,us in plotmap.items():
                if us in info:
                    opts[bok] = info[us]
            for axis in ['x','y']:
                _opt = f"{axis}_label"
                if _opt in info:
                    opts[_opt] = info[_opt]

            # make figure
            f = bokeh.plotting.figure(**opts)

            # options that require function calls
            if 'font' in info:
                f.xaxis.axis_label_text_font = info['font']
                f.yaxis.axis_label_text_font = info['font']
            if 'grid_line_color' in info:
                f.xgrid.grid_line_color = info['grid_line_color']
                f.ygrid.grid_line_color = info['grid_line_color']

            # set axis properties
            for ax in ('x','y'):
                axis = f"{ax}axis"
                if axis in info:
                    for k,v in info[axis].items():
                        exec(f"f.{axis}.{k} = info[axis][k]")

            # special cases
            for axis in ('x','y'):
                _opt = f"{axis}_range"
                if _opt in info and info[_opt] != None:
                    _string = f"f.{axis}_range = bokeh.models.DataRange1d(start={info[_opt][0]}, end={info[_opt][1]})"
                    exec(_string)
                _opt = f"{axis}_range_flipped"
                if _opt in info and info[_opt] != None:
                    exec(f"f.{axis}_range.flipped={info[_opt]}")

            # set the figure in the figures dict
            print(f"Set figure {info['name']}")
            figures[info['name']] = f

    if 'output_mode' in plots['main']:
        figures['output_mode'] = plots['main']['output_mode']

    # return the figures dict
    return figures

_link_plots_count = 0
def link_plots(
        main_figure = None,
        main_renderer = None,
        renderers = None,
        linkdatas = None):
    """
    Function to link sub-plots to the main plot so we can hover
    the mouse over the main plot to update the sub-plots.

    Input:
    * main_figure: the main Bokeh figure
    * main_renderer: the main Bokeh renderer ('HRD' or 'CMD')
    * renderers: dict of all renderers
    * linkdata: dict of data for each sub-plot

    Output:
    none
    """

    # make list of sub-renderers
    sub_renderers  = [v for k,v in renderers.items() if k != main_renderer]

    # link datasets to javascript code
    codes = []
    args = {}

    # we use a global var, _link_plots_count, to avoid
    # two variables having the same name in javascript
    global _link_plots_count
    for (sub_renderer,linkdata) in zip(sub_renderers,linkdatas):
        _link_plots_count += 1
        codes.append(f"renderer{_link_plots_count}.data = linkdata{_link_plots_count}[index]")
        args[f'linkdata{_link_plots_count}'] = linkdata
        args[f'renderer{_link_plots_count}'] = sub_renderer.data_source
        #print('CDS:' + str(linkdata))

    # javascript code to update the sub plot
    # when we hover over the main plot
    javascript = f"""
    if (cb_data.index.indices.length > 0) {{
        const index = cb_data.index.indices[0];
        {';'.join(codes)}
    }}
    """

    # construct javascript callback
    callback = bokeh.models.CustomJS(
        code = javascript,
        args = args
    );

    # add hover javascript to link plots
    main_figure.add_tools(bokeh.models.HoverTool(
        tooltips = None,
        #names = [main_renderer], # Bokeh < 3
        renderers = [renderers[main_renderer]], # Bokeh > 3
        callback = callback,
    ))
    hover_tool = main_figure.select(type=bokeh.models.HoverTool)
    #hover_tool.names = [main_renderer] # Bokeh < 3
    #hover_tool.name = main_renderer # Bokeh >= 3

    return

def make_subplots(ensemble_data=None,
                  plots=None,
                  figures=None,
                  renderers=None,
                  datasource=None):
    """
    Make the subplots and return the data to link
    them to the main plot.

    Input:
    * ensemble_data : the ensemble of data
    * plots : dict of plots (see Bokeh-HRD.py)
    * figures : dict of Bokeh figures
    * renderers  : dict of Bokeh renderers
    * datasource : Bokeh datasource containing all our data

    Output:
    * (renderers,linkdatas,colorbars) :
    *     updated renderers dict and filled linkdata and colourbars dicts
    """
    subplots = [k for k in plots.keys() if k != 'main']
    linkdatas = []
    subplot_colour_bars = {}

    for subplot in subplots:
        plot = plots[subplot]

        # find data
        _datasource = datasource.data[subplot]
        #print(f"DATA {_datasource}")

        # make new renderer
        if subplot.startswith('dist:'):
#            renderers[subplot] = figures[subplot].line(
#                line_width=1
#            )
            try:
                if renderers[subplot]:
                    new = False
            except:
                new = True

            renderers[subplot] = figures[subplot].multi_line(
                line_width='width',
                source=_datasource[0],
                line_color='colour',
                xs='xs',
                ys='ys',
                legend_field='label',
            )
            figures[subplot].legend.background_fill_alpha=0.0
            figures[subplot].legend.spacing=-3
            figures[subplot].legend.location='top_left'

        elif subplot.startswith('map:'):
            # analyse all possible subplots to determine dx,dy etc.
            if plot.get('analysis',None) == None:
                _xcol = plots['main']['xcol']
                _ycol = plots['main']['ycol']
                _dataname = plot['dataname']
                analysis = {}
                if _xcol in ensemble_data:
                    for _x in ensemble_data[_xcol]:
                        if _ycol in ensemble_data[_xcol][_x]:
                            for _y in ensemble_data[_xcol][_x][_ycol]:
                                if _dataname in ensemble_data[_xcol][_x][_ycol][_y]:
                                    analysis = analyse_2D(ensemble_data[_xcol][_x][_ycol][_y][_dataname],
                                                          analysis,
                                                          roundfunc=plot.get('roundfunc',None))
                plot['analysis'] = analysis
            else:
                analysis = plot['analysis']

            # choose box size dx,dy
            if plot['dx'] == None:
                if plot['rebinx'] is not None:
                    dx = plot['rebinx']
                else:
                    dx = analysis['dx']
            else:
                dx = plot['dx']

            if plot['dy'] == None:
                if plot['rebiny'] is not None:
                    dy = plot['rebiny']
                else:
                    dy = analysis['dy']
            else:
                dy = plot['dy']

            # make map colours
            colours = []
            zmin = analysis['zmin']
            zmax = analysis['zmax']

            # normalize?
            if plot['normalize_to_zmax']:
                zmin /= zmax
                zmax = 1.0

            # log scale
            if plot['logz']:
                zmin = math.log10(zmin)
                zmax = math.log10(zmax)

            # limit ranges
            if plot['limit_range_z'] and plot['limit_range_z'][0]:
                zmin = plot['limit_range_z'][0]
            if plot['limit_range_z'] and plot['limit_range_z'][1]:
                zmax = plot['limit_range_z'][1]

            # hence colour bar
            deltaz = zmax - zmin
            dz = deltaz/plot['ncolour_bar']
            z = zmin
            k = plot.get('colour_bar_index',0.5) # power of colour drop off (smaller -> faster)
            while z <= zmax:
                _x = int(255*(1.0-((z-zmin)/deltaz))**k)
                colours.append(colormap.rgb2hex(_x,_x,255))
                z += dz

            colour_mapper = bokeh.models.LinearColorMapper(palette=colours,
                                                           low=zmin,
                                                           high=zmax)

            renderers[subplot] = figures[subplot].rect(
                fill_color = {
                    'field' : 'z',
                    'transform' : colour_mapper
                },
                width = dx,
                height = dy,
                width_units = 'data',
                height_units = 'data',
                line_color = None,
                dilate = True,
            )
            colour_bar = bokeh.models.ColorBar(
                color_mapper=colour_mapper,
                location=(0, 0),
                ticker=bokeh.models.BasicTicker(desired_num_ticks=len(colours))
            )
            colour_bar.margin = 3
            colour_bar.padding = 10
            colour_bar.label_standoff = 0
            colour_bar.width = 10
            colour_bar.height = 'auto'
            colour_bar.syncable = False
            subplot_colour_bars[subplot] = colour_bar
        else:
            print("unknown subplot type (subplot is {subplot}) should be dist or map")
            sys.exit(1)
        linkdatas.append(_datasource)

    return (renderers,linkdatas,subplot_colour_bars)

def output_plot(figures=None,
                renderers=None,
                subplot_colour_bars=None,
                filename=None,
                zstats=None,
                plots=None,
                disttype=None,
                title='binary_c ensemble',
                scrollbars=True,
                do_range_slider=False,
                allow_CORS=False):
    """
    Plot output to HTML function.

    We put the first plot (HRD) on the left
    the rest in a stack two-wide to the right of this.

    Input:
    * disttype : one of 'HRD' or 'CMD'
    * figures : dict of Bokeh figures
    * renderers : dict of renderers
    * subplot_colour_bars : dict of colour bars of sub plots
    * filename : HTML output filename
    * plots : dict of plots (see Bokeh-hrd.py)
    * title : HTML title
    * scrollbars : if True we construct the page ourselves including
                   scrollbars. Recommended.
    * allow_CORS : if True, disable security to allow local Javascript
                   files to be loaded. Might be necessary (sorry).

    Output:
    Writes the HTML to filename.
    """
    output_mode = figures.get('output_mode',
                              'inline')
    figs = list([v for k,v in figures.items() if k!='output_mode'])

    for subplot,cbar in subplot_colour_bars.items():
        figures[subplot].add_layout(cbar,'right')

    if scrollbars == False:
        # output with no scrollbars
        first = figs.pop(0)
        bokeh.io.output_file(filename,
                             title=title,
                             mode=output_mode)
        bokeh.plotting.save(bokeh.layouts.row(first,
                                              bokeh.layouts.grid(figs,
                                                                 ncols=2)),
                            resources=None
                            )
    else:
        # plot with subplots in scrollbars
        if allow_CORS == False:
            anon = "crossorigin=\"anonymous\""
        else:
            anon = ""

        min_height = 100000
        for plot in plots:
            if plot != 'main' and 'height' in plots[plot]:
                min_height = min(min_height,
                                 plots[plot]['height'])


        """

    // map fx,fy to rgb colour
    function maptoRGB(fx,fy) {{
            const z = fx*fy;
            const r = 255 * (1.0 - z);
            const g = 255 * (1.0 - fx);
            const = 255 * (1.0 + z - fx);
            return (r,g,b);
}}

    // map rgb to hex colours
    function RGBToHex(r,g,b) {{
  r = r.toString(16);
  g = g.toString(16);
  b = b.toString(16);

  if (r.length == 1)
    r = "0" + r;
  if (g.length == 1)
    g = "0" + g;
  if (b.length == 1)
    b = "0" + b;

  return "#" + r + g + b;
}}

    // test if y is in range [miny,maxy]
    function inrange(y,miny,maxy) {{
            return Boolean(true);
            }}

    // map (x,y) point to colour
    function maptohex(x,y) {{
            const fx = x;
            const fy = y;
            if (inrange(y,0,1)) {{
              return RGBToHex(maptoRGB(fx,fy));
            }}
            else
            {{
              return "#FFFFFF";
            }}
}}
        """
        with open(filename,'w') as file:
            file.write(f"""<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>binary_c ensemble</title>
    <script type="text/javascript" src="js/bokeh-{bokeh.__version__}.min.js"
        ></script>
    <script type="text/javascript" src="js/bokeh-widgets-{bokeh.__version__}.min.js"
        {anon}></script>
    <script type="text/javascript" src="js/bokeh-mathjax-{bokeh.__version__}.min.js"
        {anon}></script>
    <script type="text/javascript" src="js/bokeh-tables-{bokeh.__version__}.min.js"
        {anon}></script>
    <script type="text/javascript" src="js/bokeh-gl-{bokeh.__version__}.min.js"
        {anon}></script>
    <script type="text/javascript">
        Bokeh.set_log_level("info");
    </script>
<style>
            .main-row {{
display: grid;
grid-template-columns: {plots['main']['height']}px {plots['main']['height']+20}px;
height: {plots['main']['height']}px;
}};

            .main-row-element {{
display: inline-grid;
}}
            .main-row-scrolled-element {{
display: inline-grid;
height: 100vh;
}}

            .subplot-grid {{
display: grid;
grid-template-columns: 1fr 1fr;
overflow: scroll;
}};
            .sub-plot-element {{
display: inline-grid;
}};
    </style>

<style>
#watermark {{
  position: fixed;
  bottom: 10px;
  right: 10px;
  z-index: 999;
  opacity: 0.5;
  text-align: right;
  color: red;
}}
</style>

  </head>

<div id="watermark"><A href="https://binary_c.gitlab.io/"><img width=100 height=100 src="https://binary_c.gitlab.io/images/logo3a-800.png"></a></div>
""")

            # plot components list
            components = list(figs)

            if do_range_slider:
                    # add range slider
                _range_slider = range_slider(
                    main_figure = figures[disttype],
                    main_renderer = renderers[disttype],
                    zstats = zstats
                )
                components.insert(0,_range_slider)

            # main plot
            (script,divs) = bokeh.embed.components(components)
            file.write(script)

            # set up main row
            file.write("""\n<div class="main-row"><div>\n""")

            divs = list(divs)
            if do_range_slider:
                _range_slider = divs.pop(0)
            main = divs.pop(0)
            main = main.replace("bk-root","bk-root main-plot")
            file.write("""<div class="main-row-element">""")
            if do_range_slider:
                file.write(_range_slider)
            file.write(main)
            file.write("</div>")

            file.write("</div><div>") # main-row divider

            file.write("""<div class="main-row-scrolled-element">""")
            file.write("""\n<div class="subplot-grid">\n""")
            for div in divs:
                div = div.replace("bk-root","bk-root sub-plot-element")
                file.write(div)
                file.write("\n")

            file.write("</div>") # subplot
            file.write("</div>") # main-row element
            file.write("</div>") # main row

            # close HTML
            file.write("""
  </body>
</html>
""")
            file.close()

    print(f"See {filename}")

def stellar_track(func,mmin=0.07,mmax=200,n=100):
    """
    Return a dict containing the a stellar-evolution track.

    The function, func(m), is a function of mass, m, and should return
    a tuple (Teff,L,g).

    Input:
    * func : function, see MS_HRD_H and MS_HRD_He for examples.
    * mmin : minimum mass (e.g. 0.07 for H, 0.15 for He)
    * mmax : maximum mass (e.g. 200 for H, 10 for He)
    * n : number of masses to be sampled (>=100 is good)

    Output:
    * returns a dict of lists of values returned by func(m)
    {'logTeff' : [], 'logL' : [], 'logg' : []}
    """
    lm = math.log10(mmin)
    dlm = (math.log10(mmax) - lm)/n
    data = {'logTeff' : [], 'logL' : [], 'logg' : []}
    while lm<math.log10(mmax):
        m = 10.0**lm
        (Teff,L,g) = func(m)
        data['logTeff'].append(Teff)
        data['logL'].append(L)
        data['logg'].append(g)
        lm += dlm
    for x in data:
        data[x] = np.log10(data[x])
    return data

def Teff(L,R):
    """
    Function to convert from luminosity, L, and radius, R, to effective temperature.

    Input:
    * L : Stellar luminosity (in solar units)
    * R : Stellar radius (in solar units)

    Output:
    * Effective temperature (Kelvin)
    """
    Teff = ((L*Lsun)/(kSB * (R*Rsun)**2))**0.25
    return Teff

def ZAMS_HRD_H(m):
    """
    Function to evaluate zero-age main sequence location of
    a hydrogen-burning star of mass m, given Z=0.02.
     (from Tout et al., 1996, MNRAS, 281, 257, binary_c version)

    Input :
    * m = Mass (in solar unit)

    Output :
    (Teff,L,g) tuples (units: Kelvin, Lsun, gsun)
    """
    main_sequence_parameters = (0, 0.397042, 8.52763, 0.00025546, 5.43289, 5.56358, 0.788661, 0.00586685, 1.71536, 6.59779, 10.0885, 1.01249, 0.0749017, 0.0107742, 3.08223, 17.8478, 0.00022582)

    m2 = m*m;
    m3 = m2*m;
    m5 = m3*m2;
    m8 = m5*m3;
    m05 = math.sqrt(m); # m^0.5
    m25 = m05*m2; # m^2.5
    m65 = m25*m2*m2; # m ^ 6.5
    m90 = m25*m65; # m ^ 9
    m190 = m90*m90*m; # m ^ 19
    m195 = m190*m05; # m ^ 19.5

    L = (main_sequence_parameters[1]*m5*m05 +
         main_sequence_parameters[2]*m8*m3)/ (
             main_sequence_parameters[3] +
             m3 +
             main_sequence_parameters[4]*m5 +
             main_sequence_parameters[5]*m5*m2 +
             main_sequence_parameters[6]*m8+
             main_sequence_parameters[7]*m8*m*m05);

    R = (main_sequence_parameters[8]*m25 +
         main_sequence_parameters[9]*m65 +
         main_sequence_parameters[10]*m2*m90 +
         main_sequence_parameters[11]*m190 +
         main_sequence_parameters[12]*m195)/(
             main_sequence_parameters[13] +
             m2*(main_sequence_parameters[14] +
                 m65*(main_sequence_parameters[15] +
                      m90*m)) +
             main_sequence_parameters[16]*m195);

    _r = R * Rsun
    g = G * (m*Msun) / (_r*_r)

    return (Teff(L,R),L,g);

def ZAMS_HRD_He(m):
    """
    Function to evaluate zero-age main sequence location of
    a helium-burning star of mass m, given Z=0.02.
     (from Hurley et al. 2000/2202, binary_c version)

    Input :
    * m = Mass (in solar unit)

    Output :
    (Teff,L,g) tuples (units: Kelvin, Lsun, gsun)
    """
    m15 = m**1.5
    L = 1.5262e4 * m**10 * m**1.25/( 0.0469 + m**6 * ( 31.18 + m15*( 29.54 + m15 )))
    R = 0.23910* m**4.6 /(0.00650 + (0.1620 + m) * m**3)
    _r = R * Rsun
    g = G * (m*Msun) / (_r*_r)
    return (Teff(L,R),L,g);

def ZAMS_plot(figures,yaxis):
    """
    Function to add ZAMS tracks to the HRD.

    Input:
    * figures : list of Bokeh figures
    * yaxis : the choice of yaxis ("logL" or "logg")

    Output:
    none
    """
    data = stellar_track(ZAMS_HRD_H,mmin=0.07,mmax=200)
    figures['HRD'].line(data['logTeff'],
                        data[yaxis],
                        name='H ZAMS',
                        legend_label="H ZAMS",
                        line_color='#000000',
                        line_width=3)
    data = stellar_track(ZAMS_HRD_He,mmin=0.15,mmax=10)
    figures['HRD'].line(data['logTeff'],
                        data[yaxis],
                        name='He ZAMS',
                        legend_label="He ZAMS",
                        line_dash='dashed',
                        line_color='#000000',
                        line_width=3)

def constant_radius_lines(plots,
                          figures,
                          analysis):

    """
    Make lines of constant radius for the HRD.

    Input:
    * plots : dict of plots (see Bokeh-HRD.py)
    * figures : dict of Bokeh figures
    * analysis : data analysis

    Returns:
    nothing
    """

    ycol = plots['main']['ycol']
    opts = plots['main']['radii']
    paddingf = opts['paddingf']
    paddinglow = opts['paddinglow']
    lowcount = 0
    if ycol == 'logL':
        xmin = analysis['xmin'] - analysis['dx']*opts['paddingf']
        xmax = analysis['xmax'] + analysis['dx']*opts['paddingf']
        ymin = analysis['ymin'][ycol] - analysis['dy'][ycol]*opts['paddingf']
        ymax = analysis['ymax'][ycol] + analysis['dy'][ycol]*opts['paddingf']

        for logR in np.arange(opts['loghigh'],opts['loglow'],opts['logstep']):
            R = 10.0**logR
            logTeff = np.linspace(xmin,
                                  xmax,
                                  opts['npoints']+1,
                                  endpoint=True)
            logL = np.log10(kSB * (R*Rsun)**2 * (10.0**logTeff)**4 /Lsun)
            condition = ((logL >= ymin) & (logL <= ymax))
            logTeff = logTeff[condition]
            if np.size(logTeff) > 0:
                logL = logL[condition]

                # add line
                figures['HRD'].line(logTeff,
                                    logL,
                                    line_color=opts['colour'],
                                    alpha=opts['alpha'])

                on_lower_edge = (logL[0] <= ymin + paddinglow)



                # add label : note that, at present, Bokeh
                # cannot do LaTeX-style formatting or MathML
                # in labels
                x = logTeff[0]
                y = logL[0]
                if on_lower_edge:
                    lowcount += 1
                    y -= lowcount*paddinglow + opts['lowoffset']

                figures['HRD'].add_layout(bokeh.models.Label(x=x,
                                                             y=y,
                                                             text_align='center',
                                                             text=f"{R:g}\nR⊙"))

def stellar_type_list():
    """
    Return a list of possible stellar types
    """
    return stellar_type_label_dict.values()

def stellar_type_label_dict():
    """
    Return a dict of stellar types { number : string }
    """
    return {
        0:'LMMS',
        1:'MS',
        2:'HG',
        3:'GB',
        4:'CHeB',
        5:'EAGB',
        6:'TPAGB',
        7:'HeMS',
        8:'HeHG',
        9:'HeGB',
        10:'HeWD',
        11:'COWD',
        12:'ONeWD',
        13:'NS',
        14:'BH',
        15:''
    }

def fix_data(hrd,plots,analysis=None):
    """
    Ensemble data might need cleaning up. Do it here.

    Input:
    * hrd : ensemble data
    * plots : dict of plots (see Bokeh-HRD.py)
    * analysis : data analysis dict
    """

    newhrd = { 'logTeff' : {} }
    if analysis == None:
        analysis = analyse_4D(hrd)
    dlogg = analysis['dy'].get('logg',None)
    fix_loggs = dlogg != None and plots['main'].get('fix_g_units',False)

    # strip very low log Teffs
    for logTeff in hrd['logTeff']:
        if logTeff >= 2.5 and logTeff <= 7.0:
            # early datasets gave log10(log10(g)) by mistake, so correct for that
            if fix_loggs and 'logg' in hrd['logTeff'][logTeff]:
                for logg in sorted(hrd['logTeff'][logTeff]['logg']):
                    fixed_logg = bin_data(10.0**logg,dlogg)
                    newhrd['logTeff'][logTeff] = hrd['logTeff'][logTeff]
                    newhrd['logTeff'][logTeff]['logg'][fixed_logg] = newhrd['logTeff'][logTeff]['logg'].pop(logg)
            else:
                newhrd['logTeff'][logTeff] = hrd['logTeff'][logTeff]


    return newhrd

def xaxis_HTML_label(xaxis):
    """
    Return an x-axis label in HTML

    Input:
    * xaxis : either "logTeff" or "GBP-GRP"

    Output:
    * HTML string
    """
    if xaxis == 'logTeff':
        return r'log<sub>10</sub>(<i>T</i><sub>eff</sub>/K)'
    elif xaxis == 'YBC GBP-GRP':
        return r'<i>G</i><sub>BP</sub>-<i>G</i><sub>RP</sub>'
    elif xaxis == 'log timescale / d':
        return r'log<sub>10</sub>(<i>t</i><sub>plateau</sub>/d)'
    elif xaxis.startswith('JWST'):
        return xaxis.replace('JWST','')
    else:
        return 'unknown xaxis'

def yaxis_HTML_label(yaxis):
    """
    Return a y-axis label in HTML

    Input:
    * yaxis : either "logL" or "logg"

    Output:
    * HTML string
    """
    if yaxis == 'logL' or yaxis == 'log luminosity / Lsun':
        return r'log<sub>10</sub>(<i>L</i>/L<sub>&#9737;</sub>)'
    elif yaxis == 'logg':
        return r'log<sub>10</sub>(<i>g</i>)'
    elif yaxis == 'YBC G':
        return r'<i>G</i>'
    elif yaxis.startswith('JWST'):
        return yaxis.replace('JWST','')
    else:
        return 'unknown yaxis'

def xaxis_label(xaxis):
    """
    Return a x-axis label as Bokeh LaTeX/mathjax-style string

    Input:
    * xaxis : either "logTeff" or "GBP-GRP"

    Output:
    * Bokeh LaTeX/mathjax-style string
    """
    if xaxis == 'logTeff':
        return r'$$\log_{10}(T_\mathrm{eff}/\mathrm{K})$$'
    elif xaxis == 'YBC GBP-GRP':
        return r'$$G_\mathrm{BP}-G_\mathrm{RP}$$'
    elif xaxis == 'log timescale / d':
        return r'$$\log_{10}(t/\mathrm{d})$$'
    elif xaxis.startswith('JWST'):
        return xaxis.replace('JWST','')
    else:
        return 'unknown xaxis'

def yaxis_label(yaxis):
    """
    Return a y-axis label as Bokeh LaTeX/mathjax-style string

    Input:
    * yaxis : either "logL" or "logg"

    Output:
    * Bokeh LaTeX/mathjax-style string
    """
    if yaxis == 'logL' or yaxis == 'log luminosity / Lsun':
        return r'$$\log_{10}(L/\mathrm{L}_\odot)$$'
    elif yaxis == 'logg':
        return r'$$\log_{10}(g)$$'
    elif yaxis == 'YBC G':
        return r'$$G$$'
    elif yaxis.startswith('JWST'):
        return yaxis.replace('JWST','')
    else:
        return 'unknown yaxis'

def tooltips_string(xaxis,yaxis):

    """
Function to return y-axis label for tooltips (i.e. HTML style)
    Input:
    * xaxis : "logTeff", "YBC GBP-GRP" etc.
    * yaxis : "logL", "logg", "G" etc.

    Output:
    * returns HTML tooltip string

"""
    xlabel = xaxis_HTML_label(xaxis)
    ylabel = yaxis_HTML_label(yaxis)
    if yaxis == 'YBC G':
        return f"""<div>{xlabel}=$x, {ylabel}=$y,  log<sub>10</sub>(<i>p</i>)=@formatted_probability : <i>f</i><sub>bin</sub>=@binfrac</div>"""
    else:
        return f"""<div>{xlabel}=$x, {ylabel}=$y,  log<sub>10</sub>(<i>p</i>)=@formatted_probability : <i>f</i><sub>bin</sub>=@binfrac</div>"""

def flipxaxis(xaxis):

    """
    Function to automatically decide whether to flip the y axis

    Input:
    * xaxis ("logTeff" or "YBC GBP-GRP")

    Output:
    * Boolean
    """

    if xaxis == 'logTeff':
        r = True
    else:
        r = False
    return r

def default_xrange(xaxis):
    if xaxis == 'logTeff':
        r = [5.5,3]
    elif xaxis == 'YBC GBP-GRP':
        r = [-1,5]
    elif xaxis == 'log timescale / d':
        r = [0,4]
    elif xaxis.startswith('JWST'):
        ranges = {
            'JWST F090W-F150W' : [-1,4],
            'JWST F090W-F360M' : [0,9],
            'JWST F090W-F250M' : [-1,6],
            'JWST F090W-F277W' : [0,5.5]
        }
        r = ranges.get(xaxis,[-1,9])
    else:
        r = [5.5,3]
    return r

def flipyaxis(yaxis):

    """
Function to automatically decide whether to flip the y axis

    Input:
    * yaxis ("logL" or "logg")

    Output:
    * Boolean
    """
    if yaxis == 'logg' or \
       yaxis == 'YBC G' or \
       (yaxis.startswith('JWST') and not '-' in yaxis):
        return True
    else:
        return False

def default_yrange(yaxis):
    if yaxis == 'logL' or yaxis == 'log luminosity / Lsun':
        r = [-3,7]
    elif yaxis == 'YBC G':
        r = [18,-4]
    elif yaxis.startswith('JWST'):
        ranges = {
            'JWST F090W-F480M' : [0,9],
            'JWST F090W-F430M' : [0,9],
            'JWST F090W-F444W' : [0,5.5]
        }
        r = ranges.get(yaxis,[6,-6])
    else:
        r = None
    return r

def range_slider(main_figure=None,
                 main_renderer=None,
                 zstats=None):
    """
Make range slider linked to probability.
    """
    range_slider = bokeh.models.RangeSlider(
        start=zstats['x']['low'],
        end=zstats['x']['high'],
        value=(zstats['x']['low'],
               zstats['x']['high']),
        title='log(p) range',
        name='HRD log probability range'
    )

    if False:
        range_slider.js_link(
            'value',
            main_figure.x_range,
            'start',
            attr_selector=0
        )
        range_slider.js_link(
            'value',
            main_figure.x_range,
            'start',
            attr_selector=1
        )

    print(main_renderer.data_source.data.keys())
    exit()
    if True:
# https://discourse.bokeh.org/t/heatmap-color-update-from-js-on-change-should-something-be-specified-in-linearcolormapper/5250/2
        range_slider.js_on_change(
            'value',
            bokeh.models.CustomJS(
                args = dict(
                    renderer=main_renderer,
                    fig=main_figure,
                    data=main_renderer.data_source
                ),
                code="""
    const data = renderer.data_source.data['probability'];
    renderer.glyph.fill_alpha =
    fig.reset.emit();

"""))

#transform.low = Math.min(...data);
#    transform.high = Math.max(...data);


    return range_slider
