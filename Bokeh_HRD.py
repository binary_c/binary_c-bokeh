#!/usr/bin/env python3

############################################################
#
# Make interactive Hertzsprung-Russell diagram using binary_c
# ensemble data made with binary_c and libcdict.
#
############################################################

import binarycpython
from binarycpython.utils.ensemble import load_ensemble
import bokeh
import ensemble_bokeh
import math
import traceback
import json
import sys
vb = True # verbose output flag
dist = 'CMD Gaia' # 'HRD', 'CMD Gaia' or 'CMD JWST'

############################################################
# set up plots
############################################################

if dist == 'HRD':
    # HRD
    plot_title = "Hertzsprung-Russell diagram"
    xcol = 'logTeff'
    yaxis = 'logL' # 'logL' or 'logg'
    xlabel_html = r'log<sub>10</sub>(<i>T</i><sub>eff</sub>/K)'
    subplots = True
    xrange = [3,7]
    zams = True
    rebin = None # none required for HRD?
    filename = f'{dist}.html'

elif dist == 'CMD Gaia':
    # CMD for Gaia
    plot_title = "Gaia Colour-Magnitude Diagram"
    xcol = 'YBC GBP-GRP'
    yaxis = 'YBC G'
    xlabel_html = xcol
    subplots = True
    xrange = [1,2]
    zams = False
    rebin = [0.15,0.15] # rebin [x,y] data to these binwidths [0.15,0.15]
    #rebin = [0.10,0.10]
    filename = f'{dist}.html'

elif dist == 'CMD JWST':
    # CMD for JWST
    plot_title = "JWST Colour-Magnitude Diagram"

    # choose one of the plots from Fig 1 of
    # https://iopscience.iop.org/article/10.3847/2515-5172/acbb72#references
    #xcol = 'JWST F090W-F150W'; yaxis = 'JWST F090W';
    #xcol = 'JWST F090W-F360M'; yaxis = 'JWST F090W-F480M';
    #xcol = 'JWST F090W-F250M'; yaxis = 'JWST F090W-F430M';
    xcol = 'JWST F090W-F277W'; yaxis = 'JWST F090W-F444W'
    xlabel_html = xcol
    subplots = True
    xrange = [0,4]
    zams = False
    rebin = [0.05,0.05] # rebin [x,y] data to these binwidths [0.15,0.15]
    #rebin = [0.10,0.10]
    filepost = xcol.removeprefix('JWST ') + '_' + yaxis.removeprefix('JWST ')
    filename = f'{dist} {filepost or ""}.html'
print(f"Making HTML in \"{filename}\"")

disttype = dist.split()[0] # 'CMD' or 'HRD'
plots = {
    'main' :
    {
        'name' : disttype,
        'xcol' : xcol,
        'xlabel_html' : xlabel_html,
        'ycol' : yaxis,
        'ylabel_html' : ensemble_bokeh.yaxis_HTML_label(yaxis),
        'zcol' : 'probability',
        'title' : plot_title,
        'font' : 'Helvetica',
        'width' : 800,
        'height' : 800,
        'xaxis' : {
            'axis_label' : ensemble_bokeh.xaxis_label(xcol),
        },
        'yaxis' :{
            'axis_label' : ensemble_bokeh.yaxis_label(yaxis),
        },
        'x_range' : ensemble_bokeh.default_xrange(xcol),
        'y_range' : ensemble_bokeh.default_yrange(yaxis),
        'x_axis_location' : "below",
        'x_range_flipped' : ensemble_bokeh.flipxaxis(xcol),
        'y_range_flipped' : ensemble_bokeh.flipyaxis(yaxis),
        'logzdata' : True,
        'tooltips' : ensemble_bokeh.tooltips_string(xcol,yaxis),
        'output_mode' : 'inline', # 'inline' (all local, larger file) or 'cdn' (loads javascript from remote source) or 'local' (use local copy of cdn files)

        'subplots' : subplots,
        'ZAMS' : zams,
        'radii' : {
            # lines of constant radius
            'paddingf' : 1.0, # generic padding so the labels are not on the plot
            'paddinglow' : 0.05, # points along the x axis are shifted by this * point number [0.2]
            'lowoffset' : 0.7, # + this [0.1]
            'loghigh' : +5, # log10(high radius) [+5]
            'loglow' : -4, # log10(low radius) [-4]
            'logstep' : -1, # step (should be <0, [-1]
            'npoints' : 1000, # number of Teff points to sample [1000]
            'colour' : '#000000', # line colour [#000000]
            'alpha' : 0.5, # line alpha [0.5]
            },

        'limit_range_x' : xrange,
        'fix_g_units' : False, # should no longer be required

        # data massage
        'rebin' : rebin,
        'zdex' : 6, # show only top 6 dex (requires logzdata)
    },
    'dist:m1' :
    {
        'name' : 'dist:m1',
        'dataname' : 'm1dist',
        'title' : 'log(Primary mass) distribution',
        'width' : 400,
        'height' : 400,
        'x_range' : (-2,+2.3),
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(M_1/\mathrm{M}_\odot)$$',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}log_{10}(M_1/\mathrm{M}_\odot)$$',
        },
        'tooltips' : None,
        'dx' : 0.1,
        'disttype' : 'float',
    },
    'dist:m2' :
    {
        'name' : 'dist:m2',
        'dataname' : 'm2dist',
        'title' : 'log(Secondary mass) distribution',
        'width' : 400,
        'height' : 400,
        'x_range' : (-2,+2.3),
        #'y_range' : (0,2),
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(M_2/\mathrm{M}_\odot)$$',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}log_{10}(M_2/\mathrm{M}_\odot)$$',
        },
        'tooltips' : None,
        'dx' : 0.1,
        'disttype' : 'float',
    },
    'dist:st1' :
    {
        'name' : 'dist:st1',
        'dataname' : 'st1dist',
        'title' : 'Primary stellar type distribution',
        'width' : 400,
        'height' : 400,
        'x_range' : (0,15),
        'tooltips' : None,
        'dx' : 1,
        'disttype' : 'int',
        'xaxis': {
            'axis_label' : r'Stellar type',
            'major_label_overrides' : ensemble_bokeh.stellar_type_label_dict(),
            'major_label_orientation' : 'vertical',
            'major_label_text_font_size' : '10px',
            'major_label_policy' : bokeh.models.AllLabels(),
            'ticker' : bokeh.models.FixedTicker(ticks=list(range(0,16)))
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d\,(Stellar\,type)}$$',
        },
    },
    'dist:st2' :
    {
        'name' : 'dist:st2',
        'dataname' : 'st2dist',
        'title' : 'Secondary Stellar type distribution',
        'width' : 400,
        'height' : 400,
        'x_range' : (0,15),
        'tooltips' : None,
        'dx' : 1,
        'disttype' : 'int',
        'xaxis' : {
            'major_label_overrides' : ensemble_bokeh.stellar_type_label_dict(),
            'major_label_orientation' : 'vertical',
            'major_label_text_font_size' : '10px',
            'major_label_policy' : bokeh.models.AllLabels(),
            'ticker' : bokeh.models.FixedTicker(ticks=list(range(0,16))),
            'axis_label' : r'Stellar type',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d\,(Stellar\,type)}$$',
        },
    },
    'dist:p' :
    {
        'name' : 'dist:p',
        'dataname' : 'pdist',
        'title' : 'log(Orbital period/days) distribution',
        'width' : 400,
        'height' : 400,
        'x_range' : (math.log10(1e-3),math.log10(1e9)),
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(P_\mathrm{orb}/\mathrm{d})$$',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}\log_{10}(P_\mathrm{orb}/\mathrm{d})$$',
        },
        'tooltips' : None,
        'dx' : 0.1,
        'rebinx' : 0.5,
        'disttype' : 'float',
    },
    'dist:e' :
    {
        'name' : 'dist:e',
        'dataname' : 'edist',
        'title' : 'Eccentricity distribution',
        'width' : 400,
        'height' : 400,
        'x_range' : (0,1),
        'xaxis' : {
            'axis_label' : r'Eccentricity',
        },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{d}p/\mathrm{d}e$$',
        },
        'tooltips' : None,
        'dx' : 0.05,
        'disttype' : 'float',
    },
    'map:logP-e' :
    {
        'name' : 'map:logP-e',
        'dataname' : 'P-e map',
        'title' : r'$$\log_{10}(P_\mathrm{orb})\,-\,e\,-\,\log_{10}(N_\mathrm{stars})$$',
        'width' : 400,
        'height' : 400,
        'x_range' : (-1,9), # plot range
        'y_range' : (0,1), # plot range
        'limit_range_x' : (-1,9), # data limiter
        'limit_range_y' : (0,1), # data limiter
        'limit_range_z' : (-4,0), # data limiter (applied after logz)
        'normalize_to_zmax' : True, # divide by zmax (applied before logz)
        'logz' : True, # plot z on log scale
        'tooltips' : None,
        'disttype' : 'float',
        'dx' : None,
        'dy' : None,
        'ncolour_bar' : 100,
        'colour_bar_index' : 0.5,
        'xaxis' : {
            'axis_label' : r'$$\log_{10}(P_\mathrm{orb}/\mathrm{d})$$',
            },
        'yaxis' : {
            'axis_label' : r'$$\mathrm{Eccentricity}$$',
            },
        'roundfunc' : ensemble_bokeh.round2,
        'rebinx' : 0.5,
        'rebiny' : 0.1,
    },
}

############################################################
# Load in ensemble data and map to Bokeh ColumnDataSource
#
if vb:
    print("Load ensemble")
try:
    ensemble_file = sys.argv[1]
except:
    ensemble_file = '/var/tmp/ensemble_output.json'
all_times_data = load_ensemble(ensemble_file,quiet=True,allow_nan=True)['ensemble'][disttype + ' Bokeh']['all times']

############################################################
# make and save list of subplots
############################################################
if plots['main'].get('subplots',True):
    plots['main']['subplots'] = list(k for k in plots.keys() if k != 'main')
else:
    plots['main']['subplots'] = []

############################################################
# by stellar-type data
############################################################
all_types_data = all_times_data['stellar type primary']['all']['stellar type secondary']['all']

by_stellar_type_data = {}
if plots['main'].get('rebin',False):
    # rebin data?
    if vb:
        print("Rebin ensemble")
    r = plots['main']['rebin']
    all_types_data = ensemble_bokeh.rebin_4D(plots=plots,
                                             data=all_types_data,
                                             dx=r[0],
                                             dy=r[1],
                                             vb=vb)

for stellar_type1 in all_times_data['stellar type primary']:
    by_stellar_type_data[stellar_type1] = {}
    for stellar_type2 in all_times_data['stellar type primary'][stellar_type1]['stellar type secondary']:
        by_stellar_type_data[stellar_type1] = \
            binarycpython.utils.dicts.merge_dicts(by_stellar_type_data[stellar_type1],
                                                  all_times_data['stellar type primary'][stellar_type1]['stellar type secondary'][stellar_type2]);



# fix data for mistakes and range issues
#if dist == 'HRD':
#    all_times_data = ensemble_bokeh.fix_data(all_times_data,
#                                             plots)

if vb:
    print("Analyse ensemble data")
analysis = ensemble_bokeh.analyse_4D(all_types_data,
                                     vb=vb)

try:
    if vb:
        print("Map 4D to CDS")
    (datasource,zstats) = ensemble_bokeh.map_4D_to_ColumnDataSource(
        all_types_data,
        plots = plots,
        analysis = analysis,
        vb = False,
        by_stellar_type_data = by_stellar_type_data
    )
except Exception as e:
    print("\nFailed to load datasource")
    s,r = getattr(e, 'message', str(e)), getattr(e, 'message', repr(e))
    print ('s:', s, 'len(s):', len(s))
    print ('r:', r, 'len(r):', len(r))
    print(traceback.format_exc())
    exit()

if vb:
    print("Make figures")
# make figures and renderers dict
figures = ensemble_bokeh.make_bokeh_figures(plots)

############################################################
# make HR diagram in the main plot
#
# first, make a colour bar and add it
if vb:
    print("Make colourbar")
colour_bars = ensemble_bokeh.bokeh_colourbar(zstats = zstats)
for c in colour_bars:
    figures[disttype].add_layout(c,'right')

if vb:
    print("Add " + disttype)
# secondly, add the data as a map ("rect" type)
renderers = { disttype : figures[disttype].rect(
    x = plots['main']['xcol'], # x column
    y = plots['main']['ycol'], # y column
    fill_color = { # fill with colour_mapper (see above)
        'field' : 'hex_colours',
    },
    # "pixel" size in the distribution : you might want to increase these?
    width = analysis['dx'],
    height = analysis['dy'][plots['main']['ycol']],
    width_units = 'data',
    height_units = 'data',
    source = datasource, # data source
    line_color = None,
    dilate = True,
    name = dist,
)}

############################################################
# make the subplots and link their data to the main plot
if plots['main'].get('subplots',True):
    if vb:
        print("Make subplots")
    (renderers,linkdatas,subplot_colour_bars) = ensemble_bokeh.make_subplots(
        all_types_data,
        plots=plots,
        figures=figures,
        renderers=renderers,
        datasource=datasource
    )
else:
    renderers = []
    subplot_colour_bars = None

############################################################
# add a ZAMS track (only for the HRD)
if plots['main'].get('ZAMS',True):
    if vb:
        print("Make ZAMS")
    ensemble_bokeh.ZAMS_plot(figures,
                             yaxis)

############################################################
# add constant radius lines
if plots['main'].get('radii',None) != None and \
   xcol == 'logTeff' and \
   plots['main']['ycol'] == 'logL':
    if vb:
        print("Make radii")
    ensemble_bokeh.constant_radius_lines(plots,
                                         figures,
                                         analysis)
figures[disttype].legend.location='bottom_left'
############################################################
# make custom javascript to update the sub plot
# when hovering over the main plot
#
# Here we assume one renderer is the main (here, dist)
# and the others are subordinates
if plots['main'].get('subplots',True):
    if vb:
        print("Link plots")
    ensemble_bokeh.link_plots(
        main_figure = figures[disttype],
        main_renderer = disttype,
        renderers = renderers,
        linkdatas = linkdatas
        )

# output HTML
if vb:
    print(f"Write to {filename}")


ensemble_bokeh.output_plot(
    disttype = disttype,
    figures = figures,
    renderers = renderers,
    subplot_colour_bars = subplot_colour_bars,
    filename = filename,
    title = 'binary_c ensemble',
    plots = plots,
    allow_CORS = True, # might be insecure!
    scrollbars = True,
    zstats = zstats
)
